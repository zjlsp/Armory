# Memcache 分布式内存缓存服务器

## 安装 Memcache 缓存服务器
```
yum -y install memcached telnet telnet-server
```
## 启动 Memcache 缓存服务器
>需要启动memcached和telnet的服务器,才能正常使用Memcached

命令格式
```
service memcached [start|status|stop|restart]
service xinetd [start|status|stop|restart]
```
执行启动命令
```
[root@localhost ~]# service memcached start
[root@localhost ~]# service xinetd start
```
## 连接Memcache
命令格式:
```
telnet [memcache的服务器地址] [端口]
```
执行连接命令
```
telnet localhost 11211
```

## Memcache的add,get,set,delete命令
### add命令
命令格式
```
add [key的名称][标识符][过期的时间][字节的大小]
```
`标识符`：标识符表示内存中的节点,一般设置为0,对于php开发没有太大的意义<br />
`过期时间`：在memcached当中如果设置过期时间为0，表示永久保存，不会过期，如果设置30表示30秒后过期，过期时间以秒为单位。<br />
`字节的大小`：一般在memcached的命令行（黑窗口）当中字节大小是需要我们自己设定，比较麻烦，用byte来作为单位,对于php开发没有太大的意义
### get命令
命令格式:
```
get [key的名称]
```
### set命令
> 修改一个不存在的键值对时，相当于add添加

命令格式
```
set [key的名称][标识符][过期的时间][字节的大小]
```
### delete命令
命令格式
```
delete [key名称]
```
### 回退（退格删除）快捷键：
```
ctrl + backspace
```

### 退出memcache命令行
```
方法1：按下ctrl + ]      
方法2：在telent>输入quit
```

### 清空memcache中所有数据
```
第1种方法：重启memcached,使用service memcached restart
第2种方式：使用flush_all命令来清空
```
> 注意事项：重启telnet服务器并不会清除memcached当中数据

## 在PHP中安装Memcache的扩展
安装Memache扩展
```
[root@localhost ~]# yum install -y --enablerepo=remi --enablerepo=remi-php56 libmemcached php-pecl-memcache php-pecl-memcached
```
重启apache
```
[root@localhost ~]# service httpd restart
```
> 执行phpinfo() 查看Memcache扩展是否已经安装

### Memcached类的常用方法说明

`addServer(host,port)`:host就是linux服务器的地址，port一般就是11211(memcached的端口)，用于连接memcached服务器<br/>
`addServers(servers)`:用于连接分布式memcached服务器<br/>
`set(key,value,expire)` : 用于修改或者添加内存的memcached数据，该方法有3个参数 key键名，value是键值，exipre是过期时间，expire默认为0表示永远不过期，如果设置为秒，最大只能设置为30天的秒数(30 * 86400秒)<br/>
`get(key)`:获取对于键值对<br/>
`delete(key)`:删除memcached当中键值对<br/>

### 使用memcache实现session入库共享
```php
# 把session储存处理方式修改为memcache而不是memcached
ini_set("session.save_handler","memcache");
# 实例化 memcache
$memcached = new Memcached();
$memcached -> addServer('localhost',11211);
# 执行上方的常用方法即可操作memcache……
```
