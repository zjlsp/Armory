# y2k38时间戳

## 修改Linux系统中的时间
```bash
# 修改系统时间
[root@localhost ~]# date -s 20180326
[root@localhost ~]# date -s 20:03:00
# 硬件时间同步系统时间
[root@localhost ~]#	clock --systohc
```

## PHP代码解决y2k38问题
```php
<?
$str = "2038-12-30 00:00:00";
//var_dump(strtotime($str)); //unix时间戳
$dt = new DateTime( $str );
//转化为unix时间戳
echo $dt -> format('U'); //U的意思是Unix
```

## 要设置/etc/php.ini文件时间时区
```
data.timezone = PRC
```
重启httpd
