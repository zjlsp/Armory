# GD图像处理

```php
# 获取图片基本信息 $path文件路径
getimagesize($path);

# 获取图片的宽度信息
imagesx($resource);

# 获取图片的高度信息
imagesy($resource);

# 创建画布
imagecreate(100,100);

# 创建一个真彩色画布
imagecreatetruecolor(100,100);

# 根据一张已有的jpeg图片创建画布
imagecreatefromjpeg($path);

# 根据一张已有的gif图片创建画布
imagecreatefromgif($path);

# 根据一张已有的png图片创建画布
imagecreatefrompng($path);

# 设定图像的混色模式 参数：图片流(资源)  true则启用混色模式，否则关闭
imagealphablending($resource,true);

# 通过重采样复制和调整图像的一部分
# $dst_image 目标画布资源
# $src_image 图像画布资源
# $dst_x $dst_y 目标画布上的x和y轴坐标
# $src_x $src_y 图像画布上的x和y轴坐标
# $dst_w $dst_w 截取目标画布上的高度宽度（一般和目标画布高度宽度保持一致）
# $src_w $src_h 截取图像画布上的高度宽度（一般和图像画布高度宽度保持一致）
imagecopyresampled($dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);

# 在页面输出图像前必须要添加的header头
header('Content-type:image/jpeg');
header('Content-type:image/png');
header('Content-type:image/gif');

# 保存或输出图片文件（图片资源,图片保存路径）
# 在页面输出图像的话不需要第二个参数路径
imagejpeg($resource,$path);
imagepng($resource,$path);
imagegif($resource,$path);

# 给画布分配一个颜色(画布资源,红色基色,绿色基色,蓝色基色)
$color = imagecolorallocate($resource,255,255,255);

# 向画布填充颜色(画布资源,画布填充基点X轴,画布填充基点Y轴,已经被画布分配的颜色)
imagefill($resource,0,0,$color);

# 向画布画一个点(画布资源,起点x轴坐标,起点y轴坐标,已经被画布分配的颜色)
imagesetpixel($image, $x, $y, $color);

# 向画布画一条线条(画布资源,起点x轴坐标,起点y轴坐标,终点x轴坐标,终点y轴坐标,已经被画布分配的颜色)
imageline($resource,0,0,200,200,$color);

# 向画布画一个矩形(画布资源,左上角x轴坐标,左上角y轴坐标,右下角x轴坐标,右下角y轴坐标,已经被画布分配的颜色)
imagerectangle($resource,100,100,200,200,$color);

# 向画布画一条弧线(画布资源,圆心的x轴坐标,圆心的y轴坐标,圆形的x轴宽度,圆形的y轴宽度,弧线起始弧度,弧线终点弧度,已经被画布分配的颜色)
imagearc($resource,150,150,100,100,0,90,$color);

# 使用内置字体向画布写入文字(画布资源,字体编号,字体左下角的坐标X轴坐标,字体左下角Y轴坐标,文字内容,已经被画布分配的颜色)
imagestring($resource,10,200,200,'ABCD',$color);

# 使用ttf文件字体想画布写入文字
# (画布资源,字体字号,字体倾斜角度,字体左下角的坐标X轴坐标,字体左下角Y轴坐标,已经被画布分配的颜色,字体路径,文字内容)
imagettftext($resource,14,0,200,200,$color,$path,'ABCD');
```
