在使用UPUPW来配置thinkphp5入口文件为public目录下的时候，需要修改UPUPW的配置项

修改`E:\UPUPW_AP5.6\Apache2\conf`配置文件对应的配置项
```
<VirtualHost *:80>
    DocumentRoot "F:/lamp_www/upupw/www.thinkphp5.net/public"
	ServerName www.thinkphp51.net:80
    ServerAlias
    ServerAdmin webmaster@www.thinkphp51.net
	DirectoryIndex index.html index.htm index.php default.php app.php u.php
	ErrorLog logs/www.thinkphp51.net-error.log
    CustomLog logs/www.thinkphp51.net-access_%Y%m%d.log comonvhost
	php_admin_value open_basedir "F:/lamp_www/upupw/www.thinkphp5.net/;E:\UPUPW_AP5.6\memcached\;E:\UPUPW_AP5.6\phpmyadmin\;E:\UPUPW_AP5.6\temp\;C:\WINDOWS\Temp\"
<Directory "F:/lamp_www/upupw/www.thinkphp5.net/public">
    Options FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>
<LocationMatch "/(inc)/(.*)$">
    Require all denied
</LocationMatch>
<LocationMatch "/(attachment|attachments|uploadfiles|avatar)/(.*).(php|php5|phps|asp|asp.net|jsp)$">
    Require all denied
</LocationMatch>
</VirtualHost>
```

配置项`php_admin_value open_basedir`的文件路径原本的`F:/lamp_www/upupw/www.thinkphp5.net/public`是错误的，需要修改成
`F:/lamp_www/upupw/www.thinkphp5.net/`
