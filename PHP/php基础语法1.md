# PHP基础语法

判断变量是否定义 && 判断变量是否为空
```php
isset($var);
empty($var);
```

九大超全局预定义变量
```php
$_ENV         # 获取服务器系统相关信息
$_SERVER      # 获得服务器相关信息的
$_GET         # 获取get参数
$_POST        # 获取post参数
$_REQUEST     # 获取get或者post参数
$_SESSION     # 获取session数据
$_COOKIE      # 获取cookie数据
$_FILES       # 获取上传文件的资源
$GLOBALS      # 获取全局变量
$argv         # 在黑窗口中执行文件才会存在
$argc         # 在黑窗口中执行文件才会存在
```

常量
```php
# 常量的定义
const 常量名 = 常量值; // 旧版本写法
define('常量名','常量值');

# 常量的调用
常量名;    // 推荐写法
constant('常量名');

# 判断常量是否存在
defined('常量名');

# 获取程序中所有的常量
get_defined_constants();
```

系统常量和魔术常量
```php
PHP_VERSION     # php版本号
PHP_INT_MAX     # 整型最大值
PHP_INT_SIZE    # 整型所占字节数

__FILE__        # __FILE__文件的全路径
```

## 数据类型

### 数据类型分类

* 标量数据类型
  - 整型
  - 浮点型
  - 字符串型  
  - 布尔型
* 复合数据类型
  - 数组
  - 对象
* 特殊数据类型
  - 资源类型
  - NULL类型

### 整型
```php
abs($var)      #取一个数值的绝对值
bindec($var)   #二进制转十进制
octdec($var)   #八进制转十进制
hexdec($var)   #十六进制转十进制
decbin($var)   #十进制转二进制
decoct($var)   #十进制转八进制
dechex($var)   #十进制转十六进制
base_convert($var,n,m)  #n进制转m进制（取值范围是2到36，包括2和36）
```

### 字符串
转换函数 `implode()` `explode()` `str_split()` `join()`
```php
# 将数组合并成字符串
implode($arr,'|');
join('|',$arr);
# 将字符串切割成数组(按指定字符切割)
explode('|',$str);
# 将字符串切割成数组(按指定字符长度切割)
str_split($str,3);
```
截取函数 `trim()` `ltrim()` `rtrim()`
```php
# 将字符串左右两边空格去除
trim($str);
# 将字符串左边空格去除
ltrim($str);
# 将字符串右边空格去除
rtrim($str);
```

截取函数 `substr()` `strstr()`
```php
# 从第N个字符开始截取，截取M个字符
substr($str,n,m);
# 负数从后向前截取N个字符
substr($str,-n);
# 从指定的字符开始截取到最后一个字符
strstr($str,'截取字符串');
# true从右边开始检索，向左边截取
strstr($str,'截取字符串',true);
```

大小写转换函数 `strtolower()` `strtoupper()` `ucfirst()`
```php
# 将字符串转换成小写
strtolower($str);
# 将字符串转换成大写
strtoupper($str);
# 将字符串首字母转换成大写
ucfirst($str);
```

查找函数 `strpos()` `strrpos()`
```php
# 从左边开始检索字符串首次出现的位置
strpos($str,'检索字符串');
# 从右边开始检索字符串首次出现的位置
strrpos($str,'检索字符串');
```

替换函数 `str_replace()`
```php
str_replace('替换前字符','替换后字符',$str);
```

格式化函数 `printf()` `sprintf()`
```php
# %d表示整型，%s表示字符串
$str = 'aaaaa %s aaa %d aa';
# 直接替换输出
printf('替换第一个','替换第二个',$str);
# 替换不输出
sprintf('替换第一个','替换第二个',$str);
```

常用输出函数 `echo()` `print_r()` `var_dump()`
```php
echo($str);
print_r($str);
var_dump($str);
```

其他 `str_repeat()` `str_shuffle()` `strrev()`
```php
# 字符串重复输出N次
str_repeat('--', n);
# 将字符顺序打乱
str_shuffle($str);
# 将字符串反转
strrev($str);
```

### 类型判断相关函数

- `gettype`函数    获取数据的数据类型
- `is_numeric`函    判断是否是一个数值
- `is_int`函数    判断是否是一个整型值
- `is_bool`函数    判断是否是一个布尔型值
- `is_float`函数    判断是否是一个浮点型值
- `is_string`函数    判断是否是一个字符串型值
- `is_array`函数    判断是否是一个数组型值
- `is_object`函数    判断是否是一个对象型值
- `is_resource`函数    判断是否是一个资源型值
- `is_null`函数    判断是否是一个null类型值
