# PHP基础语法

## 流程控制
```php
if(条件){
    # 执行语句
}else if(条件){
    # 执行语句
}else{
    # 执行语句
}
```
```php
switch (variable) {
    case 'value':
        # 执行语句
        break;
    default:
        # 执行语句
        break;
}
```
```php
for ($i=0; $i < 10; $i++) {
    if ($i == 4) {
        continue;   # 结束当前循环，跳到下一次循环继续执行。
    }else if ($i == 8) {
        break;      # 中止整个循环。
    }
    echo $i;
}
```
```php
do {
    # 执行语句
} while ($a <= 10);
```

## 文件包含
```php
# 引入文件，如果引入文件时存在错误，程序不会中断，而会继续执行，并报一个警告错误
include '文件路径';
# 同上，但只会引入一次
include_once '';

# 引入文件，如果引入文件时存在错误，则程序中断执行，并显示致命错误；
require('文件路径');
# 同上，但只会引入一次
require('文件路径');
```

## 数组的相关函数

排序函数 `sort()` `asort()` `rsort()` `arsort()` `ksort()` `krsort()` `shuffle()`
```php
# 值升序排序，重新规划下标
sort($arr);
# 值升序排序，不重新规划下标
asort($arr);
# 值降序排序，重新规划下标
rsort($arr);
# 值降序排序，不重新规划下标
arsort($arr);
# 键升序排序，不重新规划下标
ksort($arr);
# 键降序排序，不重新规划下标
krsort($arr);
# 随机打乱数组，重新规划下标
shuffle($arr);
```
元素指针函数  current(), key(), next(), end(), prev(), reset(), each()

```php
# 获取当前元素指针上的值
current($arr);
# 获取当前元素指针上的键
key($arr);
# 将元素指针向下游动一位
next(&$arr);
# 将元素指针移动到最后一位
end(&$arr);
# 将元素指针向上游动一位
prev(&$arr);
# 重置元素指针到第一位
reset(&$arr);
# 获取元素指针所在的键和值以数组形式返回
each(&$arr);
```

其他函数 count(), array_push(), array_pop(), array_reverse(), in_array(), array_keys(), array_values(), list()，array_merge(), range()
```php
# 获取数组元素的个数
count($arr);
# 向数据中新增元素
array_push(&$arr, $value);
# 将数组最后一个元素弹出
array_pop(&$arr);
# 数组元素排序颠倒
array_reverse($arr);
# 判断某个值是否存在数组中
in_array($arr);
# 获取数组中的所有键返回数组
array_keys($arr);
# 获取数组中的所有值以数组形式返回
array_values($arr);
# 将数组的元素赋值给指定的变量（必须是索引类型下标）
list($v1,$v2,$v3) = $arr;
# 将两个数组合并返回新的数组
array_merge($arr1, $arr2);
# 获得自然增长的数组 a-z 0-9
range('开始值','结束值');
```

## 错误处理

### 错误级别分类

内置错误级别：`E_WARNING`、`E_NOTICE`、`E_ERROR`

用户错误级别：`E_USER_WARNING`、`E_USER_NOTICE`、`E_USER_ERROR`

### 错误的触发
手动触发
```
trigger_error('错误描述',E_USER_WARNING);
```

### 错误（显示）的设置
通过修改php.ini文件开启错误显示
```log
; 开启的错误类型(全部都开启)
error_reporting = E_ALL|E_STRICT
; 是否将错误显示到浏览器(On/显示 Off/不显示)
display_errors = On
```
替代方法 在php文件头部添加如下代码
```php
error_reporting( E_ALL & ~E_NOTICE );
```

### 错误日志设置
> 默认情况索然error_log是关闭的，但是apache会自动将程序中所报过的错误记录到apache安装根目录下的一个名为logs目录中的error.log文件中

```log
error_log = 错误日志全路径
; 是否将开启错误日志记录(On/开启 Off/不开启)
log_errors = On
```

### 关闭ob缓存
通过修改php.ini
```
;output_buffering = 4096

display_errors = On
```
### 重启apache

## 函数

### 静态变量
```php
function fnName(){
    # 该变量在函数调用后会依然存在
    static $var = 0;
}
```

### 异常处理
```php
try {
    throw new \Exception("Error Processing Request", 1);
} catch (\Exception $e) {
    echo $e;
}
```
