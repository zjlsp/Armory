# 请求

```php
<?php

namespace app\index\controller;

use \think\Request;

class New extends \think\Controller
{
    public function index(Request $request)
    {
        # 获取浏览器输入框的值
        dump($request -> domain());     // 网站的域名 https://www.baidu.com
        dump($request -> pathinfo());   // 域名携带的路径带后缀 admin.php/admin/login.html
        dump($request -> path());       // 域名携带的路径 admin.php/admin/login

        # 请求类型
        dump($request -> method());     // 获取请求类型 GET POST AJAX
        dump($request -> isGet());      // 判断是否是GET请求
        dump($request -> isPost());     // 判断是否是POST请求
        dump($request -> isAjax());     // 判断是否是AJAX请求

        dump($request -> get());        // 获取通过get方式传递过来的值 不包括url重新中传递的值
        dump($request -> param());      // 获取通过get方式传递过来的值 包括url重写传递的值
        dump($request -> post());       // 获取postt方式传递过来的参数
        dump($request -> session());    // 获取session的值
        dump();
        dump();
        dump();
    }
}
```
