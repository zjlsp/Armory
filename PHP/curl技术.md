# curl技术

> 要使用cURL库那么你的php版本里面必须包含该库的扩展,使用phpinfo查看

## cURL最重要的几个选项

| 选项                    | 描述                                   |
| ---------------------- | ------------------------------------ |
| CURLOPT_SAFE_UPLOAD    | 安全上传                                 |
| CURLOPT_URL            | 需要获取的 URL 地址                         |
| CURLOPT_RETURNTRANSFER | 是否以字符串(文本流)形式进行返回                    |
| CURLOPT_SSL_VERIFYHOST | 诉服务器当前是curl发起的请求,并没有ssl认证的服务器,一般固定为0 |
| CURLOPT_SSL_VERIFYPEER | 告诉服务器端不要校检SSL证书,一般固定为false           |
| CURLOPT_POST           | 是否使用post方式进行请求                       |
| CURLOPT_POSTFIELDS     | POST请求时需要post的数据包                    |
