# PHP扩展的安装和使用

## 安装
yum安装
```
yum install -y --enablerepo=remi --enablerepo=remi-php56 php-pecl-redis
```
重启apache
```
service httpd restart
```
重启apache后，重新在浏览器端打开phpinfo.php查看redis的扩展模块是否正确安装

在默认的情况下，redis的作用设置了一个本地访问机制,只允许黑窗口进行连接,这个机制把php的访问也阻止了，因此我们需要把本地访问机制关闭

使用vim打开/etc/redis.conf文件，找打bind 127.0.0.1这个选项如下所示

在bind前面加上#号进行注解
```
# bind 127.0.0.1
```
重启redis服务器

## 使用php连接Redis
```php
<?php
header('Content-type:text/html;charset=utf-8');
//实例化Redis
$redis = new Redis();
// 连接redis
$redis -> connect('localhost',6379);
// 如果redis开启了安全验证必须验证密码才能进入
$redis -> auth('123456');
```

## redis的各种数据类型的操作

#### 操作string和hash

```php
<?php
# …… 使用php连接Redis
// 设置String类型数据
$redis -> set('键','值');
// 获取String类型数据
echo $redis -> get('键');

// 批量设置hash类型数据
$redis -> hmset("哈希表名",[
    "字段名" => '字段值',
    "字段名" => '字段值'
]);
// 获取hash类型数据的所有字段值
echo $redis -> hgetall("哈希表名");

// 设置hash类型的单个字段数据
$redis -> hset('哈希表名',"字段名","字段值");
```

### 操作set无需集合
操作zset有序集合:在实际开发中运用不多
```
// 添加有序集合
$redis -> zadd('集合名','序号','值');
// 按序号由小到大升序排列
$redis -> zrange('集合名',0,-1);
// 按序号由小到大降序排列
$redis -> zrevrange('集合名',0,-1);
```

### redis实现消息队列
```
# 设置队列
$sickers = array(
    0 => '队列1',
    1 => '队列2',
    2 => '队列3',
);
# 循环将队列数据添加到redis中
foreach ($sickers as $value) {
    $redis -> rpush( '队列名', $value );
}

# 排队叫号过程
$sicker = $redis -> lpop('队列名');
if ( $sicker ) {
    echo $sicker;
}else{
    echo '队列为空'
}
```
