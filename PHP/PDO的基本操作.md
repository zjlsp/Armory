# PDO操作


## 基本配置和使用

连接数据库
```php
$dsn = 'mysql::host=localhost;post=3306;charset=utf8;dbname=db1';
$pdo = new PDO($dsn,'root','123456');
```

增删改操作
```php
$re = $pdo->exec('insert into table values("xxxx")');
```

查询操作
```php
$pdostatement = $pdo->query($sql);

pdo
// 以字段名为下标返回一条数据，游标向下走
$pdostatement -> fetch(PDO::FETCH_ASSOC);
$pdostatement -> fetch(PDO::FETCH_ASSOC);

// 以索引下标返回一条数据，右边向下走
$pdostatement -> fetch(PDO::FETCH_NUM);
$pdostatement -> fetch(PDO::FETCH_NUM);

// 以索引下标和字段名下标返回一条数据，右边向下走
$pdostatement -> fetch(PDO::FETCH_BOTH);
$pdostatement -> fetch(PDO::FETCH_BOTH);

// 以字段名下标返回一条数据，右边向下走（返回数据为对象）
$pdostatement -> fetch(PDO::FETCH_OBJ);
$pdostatement -> fetch(PDO::FETCH_OBJ);
// 简写方式
$PDOStatement -> fetchObject();

// 一次性取得所有的查询数据 包括索引下标和字段下标
$pdostatement -> fetchAll();

// 获取数据的总行数
$pdostatement -> rowCount();

// 获取数据的总列数
$pdostatement -> columnCount();
```

## PDO实现事务
```php

pdo
// 开启事务
$pdo -> beginTransaction();
// 提交事务
$pdo -> commit();
// 事务回滚
$pdo -> rollBack();
```

## 预处理技术
```php
$sql = 'insert into table values(:title,:content)';
$pdostatement = $pdo -> prepare($sql);
$data = array(':title' => '标题', ':content' => '内容' );
$pdostatement -> execute($data);
```

## PDO中的异常处理
```php
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
try{
    $sql = "insert into cz_usddddder (name, pwd) values ('曹操', '445566')";
    $re = $pdo->exec($sql);
}catch(PDOException $aa){
    echo '错误信息：' . $aa->getMessage() . '<br/>';
    echo '错误码值：' . $aa->getCode() . '<br/>';
    echo '错误文件：' . $aa->getFile() . '<br/>';
    echo '错误行号：' . $aa->getLine() . '<br/>';
}
```
