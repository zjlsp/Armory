# svn部署

## 安装svn

查看是否安装了低版本svn

```
rpm -qa subversion
```

卸载旧版本SVN()

```
yum remove subversion
```

安装SVN

```
yum -y install subversion
```

验证安装

```
svnserve --version
```

## 创建配置仓库

代码库创建

```
mkdir -p /data/svn
svnadmin create /data/svn
```

配置用户名密码
```
vim /data/svn/conf/passwd

# 配置项如下
# 用户名 = 密码
```

配置权限

```
vim /data/svn/conf/authz

# 个人使用，配置全部项目可读可写 其他账号没有权限
[/]
zjlsp = rw
* =
```

配置服务

```
vim /data/svn/conf/svnserve.conf

[general]
#匿名访问的权限，可以是read,write,none,默认为read
anon-access=none
#使授权用户有写权限
auth-access=write
#密码数据库的路径
password-db=passwd
#访问控制文件
authz-db=authz
#认证命名空间，subversion会在认证提示里显示，并且作为凭证缓存的关键字
# realm = My First Repository
```

## 启动进程

后台启动服务

```
svnserve -d -r /data/svn/swoole
```

查看SVN进程

```
ps -ef|grep svn|grep -v grep
```

检测SVN 端口

```
netstat -ln |grep 3690
```

停止svn服务

```
killall svnserve
```

## 设置钩子自动更新

在web环境检出svn代码,输入用户名密码测试

```
cd /var/www
svn checkout svn://localhost html
```

新建钩子处理文件

```
cd /svn/project/hooks
vim post-commit
```

编写文件如下,其实就是执行了一个sh脚本，会sh脚本的可以自行扩展

```
#!/bin/sh
DIR="/var/www/html"
export LANG=en_US.UTF-8
svn update $DIR --username admin --password 123456
```

保存文件后将改文件修改为可执行权限

```
chmod +x post-commit
```

至此，部署完成
